# $AWS_ACCESS_KEY_ID, $AWS_SECRET_ACCESS_KEY should be provided in the Deployment or Repository variables.

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 2.70"
    }
  }
}

provider "aws" {
  profile = "default"
  region  = "us-east-1"
}

data "aws_ami" "amazon-linux-2" {
 most_recent = true
 owners = ["amazon"]


filter {
   name   = "name"
   values = ["amzn2-ami-hvm*"]
 }
}

resource "aws_instance" "test" {
  ami           = data.aws_ami.amazon-linux-2.id
  instance_type = "t2.micro"

  tags = {
    Name = "TemplateHelloWorld"
  }
}